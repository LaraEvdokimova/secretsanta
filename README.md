# Secretsanta

It is necessary to create a UI autotest using Cypress to check the transactions of the draw. And bugs into the Issues chapter.

## Getting started

В  приложении https://santa-secret.ru/ создать UI автотест при помощи Сypress для проверки работы жеребьевки.

## Add your files

Репозиторий: https://gitlab.com/LaraEvdokimova/SECRETSANTA.git
Необходимо:
1) использовать своих пользователей, заранее созданных
ПОЛЬЗОВАТЕЛИ:
lara avrora lara_oth+120@icloud.com пароль: ZE8439
lara best lara_oth+121@icloud.com пароль: NN6397
lara crown lara_oth+122@icloud.com пароль: TA8581
lara demo lara_oth+123@icloud.com пароль: ET3960
lara energy lara_oth+124@icloud.com пароль: QR0422
lara fuel lara_oth+125@icloud.com пароль: UI7682

2) создать коробку
Администратор коробки: lara avrora, EMAIL: lara_oth+120@icloud.com,
КОРОБКА: avrora Идентификатор: PfUqdl

3) добавить участников коробки (кастомные команды при повторяющихся действиях)
4) запустить жеребьевку
5) в after hook сделать рефакторинг, использовать API для удаления коробки
6) баги в приложении или отклонения в поведении оформить репортами в репозиторий https://gitlab.com/LaraEvdokimova/Santasecret.git в разделе Issues и оставить падающий тест в проекте.

## Steps of the test

1) Авторизоваться на сайте https://santa-secret.ru/, используя одного из заранее созданных пользователей.
2) Перейти на страницу с жеребьевкой.
3) Выбрать коробку с идентификатором "PfUqdl".
4) Добавить участников коробки, использовать кастомные команды при повторяющихся действиях:
lara avrora lara_oth+120@icloud.com пароль: ZE8439
lara best lara_oth+121@icloud.com пароль: NN6397
lara crown lara_oth+122@icloud.com пароль: TA8581
lara demo lara_oth+123@icloud.com пароль: ET3960
lara energy lara_oth+124@icloud.com пароль: QR0422
lara fuel lara_oth+125@icloud.com пароль: UI7682
5) Нажать кнопку "Забрать коробку".
6) Проверить, что коробка успешно забрана.
7) Нажать кнопку "Провести жеребьевку".

## TEST 1

Проверить, что жеребьевка проведена успешно и выигрышный билет отображается на странице.

## TEST 2

В after hook сделать рефакторинг, использовать API для удаления коробки deactivate
репозиторий https://gitlab.com/LaraEvdokimova/SECRETSANTA.git

***

# TEST 3

Найти баг в приложении или ошибки, внести баг в репозиторий https://gitlab.com/LaraEvdokimova/Secretsanta.git в раздел Issues, оставить падающий тест в проекте Гитлаб.